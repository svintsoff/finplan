import { defineStore } from "pinia";

export const useHistoryStore = defineStore('history', {
    state: () => ({
       history: []
    }),
    getters: {
        getHistory(state) {
            return state.history
        },
        addHistoryItem(state) {
            return (type, category, cost) => {
                state.history.unshift([type, category, cost])
            }
        }
    }
})