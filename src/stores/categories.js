import { defineStore } from "pinia";

export const useCategoriesStore = defineStore('categories', {
    state: () => ({
        income: [],
        expenses: []
    }),
    getters: {
        addIncomeCategory(state) {
            return (name) => {
                state.income.push(name)
            }
        },
        addExpensesCategory(state) {
            return (name) => {
                state.expenses.push(name)
            }
        },
        getIncomeCategories(state) {
            return state.income
        },
        getExpensesCategories(state) {
            return state.expenses
        }
    }
})